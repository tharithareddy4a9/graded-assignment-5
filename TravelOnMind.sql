create database TravelOnMind;

use TravelOnMind;

create table PASSENGER (Passenger_name VARCHAR(25), Category VARCHAR(25),Gender VARCHAR(25),Boarding_City VARCHAR(25),
   Destination_City VARCHAR(25),Distance INTEGER(10),Bus_Type VARCHAR(25));
   
create table PRICE(Bus_Type VARCHAR(25),Distance INTEGER(10),Price INTEGER(20));

insert into PASSENGER values('Haritha','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into PASSENGER values('Madhu','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into PASSENGER values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into PASSENGER values('Rajitha','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into PASSENGER values('Bhaskar','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into PASSENGER values('Javeed','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into PASSENGER values('Punith','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into PASSENGER values('Shubham','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into PASSENGER values('Hemanth','AC','M','Pune','Nagpur',700,'Sitting');

insert into PRICE values('Sleeper',350,770);
insert into PRICE values('Sleeper',500,1100);
insert into PRICE values('Sleeper',600,1320);
insert into PRICE values('Sleeper',700,1540);
insert into PRICE values('Sleeper',1000,2200);
insert into PRICE values('Sleeper',1200,2640);
insert into PRICE values('Sleeper',350,434);
insert into PRICE values('Sitting',500,620);
insert into PRICE values('Sitting',500,620);
insert into PRICE values('Sitting',600,744);
insert into PRICE values('Sitting',700,868);
insert into PRICE values('Sitting',1000,1240);
insert into PRICE values('Sitting',1200,1488);
insert into PRICE values('Sitting',1500,1860);

select * from PRICE;
-- 1. How many female and how many male passengers travelled for a minimum distance of 600 KM s? 
SELECT COUNT(CASE 
                 WHEN (Gender) = 'F' THEN 1 
				 END)
                 Female FROM PASSENGER WHERE Distance >=600;
SELECT COUNT(CASE
                 WHEN (Gender) = 'M' THEN 1 
                 END)
                 Male FROM PASSENGER WHERE Distance >=600;
                 
-- 2. Find the minimum ticket price for Sleeper Bus. 
  
 SELECT MIN(price) FROM PRICE WHERE Bus_Type = 'sleeper';
 
-- 3. Select passenger names whose names start with character 'S'.

SELECT * FROM PASSENGER WHERE Passenger_name LIKE 'S%';

-- 4. Calculate price charged for each passenger displaying Passenger name, Boarding City, Destination City, Bus_Type, Price in the output.

SELECT Passenger_name , pa.Boarding_City, pa.Destination_city, pa.Bus_Type, pr.Price 
FROM passenger AS pa, price AS pr
WHERE pa.Distance = pr.Distance and pa.Bus_type = pr.Bus_type;

 -- 5. What is the passenger name and his/her ticket price who travelled in Sitting bus for a distance of 1000 KMs. 
SELECT pa.Passenger_name, pa.Boarding_city, pa.Destination_city, pa.Bus_type, pr.Price 
FROM passenger pa, price pr 
WHERE pa.Distance = 1000 and pa.Bus_type = 'Sitting';

-- 6. What will be the Sitting and Sleeper bus charge for Pallavi to travel from Bangalore to Panaji?
 SELECT DISTINCT pa.Passenger_name, pa.Boarding_city as Destination_city, pa.Destination_city as Boardng_city, pa.Bus_type, pr.Price
 FROM passenger pa, price pr
 WHERE passenger_name = 'Pallavi' and pa.Distance = pr.Distance;
 
 -- 7.List the distances from the "Passenger" table which are unique (non-repeated distances) in descending order

SELECT DISTINCT distance FROM passenger ORDER BY Distance desc;

-- 8. Display the passenger name and percentage of distance travelled by that passenger from the total distance travelled by all passengers without using user variables.
SELECT Passenger_name,  Distance * 100.0/ (SELECT SUM(Distance) FROM passenger) AS percentageOfDistance FROM passenger
GROUP BY Distance;
 
 -- 9. Create a view to see all passengers who travelled in AC Bus.

CREATE VIEW passenger_view AS SELECT * FROM PASSENGER WHERE Category = 'AC';
SELECT * FROM passenger_view;

-- 10. Create a stored procedure to find total passengers traveled using Sleeper buses.

 DELIMITER //
CREATE PROCEDURE TotalSleeperPassengers()
BEGIN
SELECT * FROM PASSENGER where Bus_Type ='Sleeper';
END //

-- 11. Display 5 records at one time

select *from travel.passenger limit 5;
 
 


